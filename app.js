var express = require('express');
var app = express();  
var server = require('http').createServer(app);  
var io = require('socket.io')(server);

app.use('/', express.static('cliente'));

server.listen(8453);
console.log('Listening on 8453');

//Config
var move = 10;
var max_players_per_map = 100;
var map_limits = {
        x: {
            max: 990,
            min: 0
        },
        y: {
            max: 690,
            min: 0
        }
    }


var mapas = new Map();
var players = new Map();

// Default Game
mapas.set(0, {
	players: new Map(),
	maxPlayers: max_players_per_map
});

var secrets = new Map();

for (var i = 0; i <= 300; i++) {
	secrets.set('DA39A3EE5E6B4B0D3255BFEF95601890AFD8070'+i, {used: false});
}

io.on('connection', function(client) {
	client.on('join', function(secret) {
		if (!secrets.has(secret)) {
			client.emit('secret_fail', 'Secret não encontrado!');
			return;
		}else if(secrets.get(secret).used) {
			client.emit('secret_fail', 'Secret usado!');
			return;
		}else{
			secrets.get(secret).used = true;
			if(mapas.get(mapas.size-1).players.size >= mapas.get(mapas.size-1).maxPlayers){
				console.log('new map');
				mapas.set(mapas.size, {
					players: new Map(),
					maxPlayers: max_players_per_map
				});
			}
			var location;
			do{
				location = {
					x: parseInt((Math.floor((Math.random() * 100) + 0)))*10,
					y: parseInt((Math.floor((Math.random() * 70) + 0)))*10
				}
			}while(!compairPLocations(mapas.get(mapas.size-1).players.entries(), location));
			var color;
			do{
				color = Math.floor(Math.random() * 16777216).toString(16);
				color = '#000000'.slice(0, -color.length) + color;
			}while(!compairPColors(mapas.get(mapas.size-1).players.entries(), color));
			var trace;
			do{
				trace = Math.floor(Math.random() * 16777216).toString(16);
				trace = '#000000'.slice(0, -color.length) + color;
			}while(!compairPTrace(mapas.get(mapas.size-1).players.entries(), trace));
			var mID = mapas.size-1;
			var player = {
				id: client.id,
				secret: secret,
				mapa: mID,
				location: location,
				premios: new Array(),
				color: color,
				trace: 'white'
			}
			players.set(player.id, player);
			mapas.get(mapas.size-1).players.set(player.id, player);
			var mapa_atual = {
				id: mapas.size-1,
				players: Array.from(mapas.get(mapas.size-1).players)
			}
			client.broadcast.emit('map'+(mapas.size-1)+'-join-start', {mapa: mapa_atual, player: player});
			client.emit('start', client.id, mapa_atual);
		}
	});

	client.on('movement', function(movement) {
		if(players.has(client.id)){
			var mapa_id = players.get(client.id).mapa;
			var player = mapas.get(mapa_id).players.get(client.id);
			console.log(player.location);
			console.log(map_limits);
			if(movement == 'up'){
				if(player.location.y > map_limits.y.min){
					console.log('ta dentro do limite!');
					mapas.get(mapa_id).players.get(client.id).location.y -= move;
					client.broadcast.emit('map'+mapa_id+'-movement', player);
					client.emit('map'+mapa_id+'-movement', player);
				}
			}else if(movement == 'down'){
				if(player.location.y < map_limits.y.max){
					mapas.get(mapa_id).players.get(client.id).location.y += move;
					client.broadcast.emit('map'+mapa_id+'-movement', player);
					client.emit('map'+mapa_id+'-movement', player);
				}
			}else if(movement == 'left'){
				if(player.location.x > map_limits.x.min){
					mapas.get(mapa_id).players.get(client.id).location.x -= move;
					client.broadcast.emit('map'+mapa_id+'-movement', player);
					client.emit('map'+mapa_id+'-movement', player);
				}
			}else if(movement == 'right'){
				if(player.location.x < map_limits.x.max){
					mapas.get(mapa_id).players.get(client.id).location.x += move;
					client.broadcast.emit('map'+mapa_id+'-movement', player);
					client.emit('map'+mapa_id+'-movement', player);
				}
			}
		}
	});
	client.on('disconnect', function () {
		console.log(client.id+" are disconnected!");
		if(players.has(client.id)){
			client.broadcast.emit('map'+players.get(client.id).mapa+'-disconnect', client.id);
			mapas.get(players.get(client.id).mapa).players.delete(client.id);
			players.delete(client.id);
		}
  	});
});


function compairPLocations(players, location) {
	for (var i = players.length - 1; i >= 0; i--) {
		var plLocation = players[i].location;
		if(plLocation.x == location.x && plLocation.y == location.y){
			return false;
		}
	}
	return true;
}
function compairPColors(players, color) {
	for (var i = players.length - 1; i >= 0; i--) {
		var plColor = players[i].color;
		if(plColor == color){
			return false;
		}
	}
	return true;
}
function compairPTrace(players, trace) {
	for (var i = players.length - 1; i >= 0; i--) {
		var pltrace = players[i].trace;
		if(pltrace == trace){
			return false;
		}
	}
	return true;
}
