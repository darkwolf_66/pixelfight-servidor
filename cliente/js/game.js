var socket = io();
var mapa, secret, ctx, canvas, my_id;

$(document).ready(function() {
    if(getSecret()){
        secret = getSecret();
        console.log(secret);
    }
    socket.emit('join', secret);
    socket.on('secret_fail', function(msg) {
        console.log(msg);
    });

    canvas = document.getElementById('game');
    canvas.width = 1000;
    canvas.height = 700;
    ctx = canvas.getContext('2d');
    socket.on('start', function(me, map) {
        my_id = me;
        console.log('Game Started..');
        mapa = map;
        for (var i = 0; i < map.players.length ; i++) {
            ctx.fillStyle = map.players[i][1].color;
            $('.players-list').append('<div class="col-md-4 player-list-id-'+map.players[i][1].id+'"><div class="player-color" style="background-color:'+map.players[i][1].color+'"></div><div>'+map.players[i][1].id+'</div></div>');
            ctx.fillRect(map.players[i][1].location.x, map.players[i][1].location.y, 10, 10);
        }
        socket.on('map'+mapa.id+'-movement', function(player) {
            removePos(ctx, getPlayer(mapa, player.id));
            setPlayer(mapa, player);
            markPos(ctx, player);
        });
        socket.on('map'+mapa.id+'-join-start', function(join) {
            mapa.players.push([join.player.id, join.player]);
            $('.players-list').append('<div class="col-md-4 player-list-id-'+join.player.id+'"><div class="player-color" style="background-color:'+join.player.color+'"></div><div>'+join.player.id+'</div></div>');
            for (var i = 0; i < join.mapa.players.length; i++) {
                ctx.fillStyle = join.mapa.players[i][1].color;
                ctx.fillRect(join.mapa.players[i][1].location.x, join.mapa.players[i][1].location.y, 10, 10);
            }
        });
        socket.on('map'+mapa.id+'-disconnect', function(player) {
            console.log('.player-list-id-'+player);
            $('.player-list-id-'+player).remove();
            removePos(ctx, getPlayer(mapa, player));
            console.log($('.player-list-id-'+player).html());
            removePlayer(mapa, player);
            for (var i = 0; i < mapa.players.length ; i++) {
                ctx.fillStyle = mapa.players[i][1].color;
                ctx.fillRect(mapa.players[i][1].location.x, mapa.players[i][1].location.y, 10, 10);
            }
        });
    });
    $(document).keydown(function(e) {
        switch(e.which) {
            case 37: // left
                socket.emit('movement', 'left');
            break;

            case 38: // up
                socket.emit('movement', 'up');
            break;

            case 39: // right
                socket.emit('movement', 'right');
            break;

            case 40: // down
                socket.emit('movement', 'down');
            break;

            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });

});
function removePos(ctx, player) {
    ctx.fillStyle = 'white';
    ctx.fillRect(player.location.x, player.location.y, 10, 10);
}
function markPos(ctx, player){
    ctx.fillStyle = player.color;
    ctx.fillRect(player.location.x, player.location.y, 10, 10);
}
function getPlayer(mapa, id){
    for (var i = mapa.players.length - 1; i >= 0; i--) {
        if(mapa.players[i][1].id == id){
            return mapa.players[i][1];
        }
    }
    return false;
}
function setPlayer(mapa, player){
    for (var i = mapa.players.length - 1; i >= 0; i--) {
        if(mapa.players[i][1].id == player.id){
            mapa.players[i][1] = player;
            return true;
        }
    }
    return false;
}
function removePlayer(mapa, id){
    for (var i = mapa.players.length - 1; i >= 0; i--) {
        if(mapa.players[i][1].id == id){
            mapa.players.splice(id, 1);
            return true;
        }
    }
    return false;
}

function getSecret() {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === 'kdf') result = decodeURIComponent(tmp[1]);
        });
    return result;
}